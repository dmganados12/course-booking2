import {Card} from 'react-bootstrap';

export default function CourseCard() {
	return(
		<Card>
			<Card.Body>
				<Card.Title>
					INSERT COURSE NAME
				</Card.Title>
				<Card.Text>
					INSERT DESCRIPTION
				</Card.Text>
				<Card.Text>
					Price: INSERT PRICE
				</Card.Text>
				<a href="/" className="btn btn-primary">
					View Course
				</a>
			</Card.Body>
		</Card>	

	)
}