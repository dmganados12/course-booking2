// Grid System and Card
import {Row, Col, Card} from 'react-bootstrap'

// Format the card with the help of utility classes bootstrap

export default function Highlights() {
    return(	
		<Row className="my-3">
			{/*1stHiglight*/}
			<Col xs={12} md={4}>
				<Card className="p-4 cardHighlight">
					<Card.Body>
						<Card.Title>Learn From Home</Card.Title>
						<Card.Text>
							Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Ea, quas?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			{/*2nd Highlight*/}
			<Col xs={12} md={4}>
				<Card className="p-4 cardHighlight">
					<Card.Body>
						<Card.Title>Study Now, Pay Later</Card.Title>
						<Card.Text>
							Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Ea, quas?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			{/*3rd Highlight*/}
			<Col xs={12} md={4}>
				<Card className="p-4 cardHighlight">
					<Card.Body>
						<Card.Title>Be Part of our Community</Card.Title>
						<Card.Text>
							Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Ea, quas?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			</Row>
		
	)
}

