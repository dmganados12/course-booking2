// Identify which components are needed to build the navigation
import { Navbar, Nav, Container } from 'react-bootstrap'

// We will now describe how we want our Navbar to look.
function AppNavBar() {
	return(
	<Navbar bg="primary" expand="lg">
		<Container>
			<Navbar.Brand>B156 Booking App</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse>
				<Nav>

				</Nav>
			</Navbar.Collapse>
		</Container>			
	</Navbar>
	);
};

export default AppNavBar;