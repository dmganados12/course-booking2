// This compoenent will used as the hero section of our page.
// Responsive -> grid system
import {Row, Col} from 'react-bootstrap';

// We will use default bootstrap utility classes to format the component.

// Create a function that will descrive the structure of the 

// 'class' -> reserved keyword (HTML)
// React/JSX elements -> 'className'
export default function Banner() {
	return(
		<Row className="p-5" >
			<Col>
				<h1>INSERT HEADING HERE</h1>
				<p className="my-4">INSERT DESC HERE</p>
				<a className="btn btn-primary" href="/">INSERT ACTION HERE</a>
			</Col>
		</Row>
		);
}

// expose component