
import AppNavBar from './components/AppNavBar';
import Banner from './components/Banner';
import Highlights from './components/Highlights'
import CourseCard from './components/CourseCard'

// JSX Component -> self closing tag
// syntax: <Element />

import './App.css';

function App() {
  return (
    <div>
      <AppNavBar />
      <Banner />
      <Highlights />
      <CourseCard />
    </div>
  );
}

export default App;
